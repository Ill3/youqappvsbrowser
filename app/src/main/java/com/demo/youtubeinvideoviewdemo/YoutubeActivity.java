package com.demo.youtubeinvideoviewdemo;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;

public class YoutubeActivity extends Activity {

	String TAG = "YoutubeActivity";
	WebView webView;
	WebAppInterface webAppInterface;
	Integer numOfVideos;


	String listOfVideoIDs;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.introlayout);
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);

	}
	public void runThis(View view){
		setContentView(R.layout.settings_layout);
	}

	public void startTests(View view) throws SQLException, ClassNotFoundException, IOException, URISyntaxException, JSONException {
		String experimentInfo = "";
		JSONObject databaseQuery = new JSONObject();
		EditText numOfVid = (EditText) findViewById(R.id.num_vid);
		if (!numOfVid.getText().toString().equals("")) {
			numOfVideos = Integer.parseInt((String) numOfVid.getText().toString());
			databaseQuery.put("num_of_videos", numOfVideos);
			experimentInfo = experimentInfo + "You are about to start an experiment of " + numOfVideos + " videos.";
			Spinner mySpinnerQuality = (Spinner) findViewById(R.id.spinner1);
			String definition = (String) mySpinnerQuality.getSelectedItem();
			if (!(definition).equals("")) {
				databaseQuery.put("definition", definition);
				experimentInfo = experimentInfo + "\nDefinition of experiment videos is set to " + definition + ".";
			}
			EditText minLength = (EditText) findViewById(R.id.min_length);
			EditText maxLength = (EditText) findViewById(R.id.max_length);
			EditText minViews = (EditText) findViewById(R.id.min_views);
			String minViewsCheck = minViews.getText().toString();
			String minLengthCheck = minLength.getText().toString();
			String maxLengthCheck = maxLength.getText().toString();
			if (!minLengthCheck.equals("")) {
				databaseQuery.put("min_length", Integer.parseInt(minLengthCheck));
				experimentInfo = experimentInfo + "\nMinimum video duration is set to " + minLengthCheck + ".";
			}
			if (!maxLengthCheck.equals("")) {
				databaseQuery.put("max_length", Integer.parseInt(maxLengthCheck));
				experimentInfo = experimentInfo + "\nMaximum video duration is set to " + maxLengthCheck + ".";
			}
			if (!minViewsCheck.equals("")) {
				databaseQuery.put("viewCount", Integer.parseInt(minViewsCheck));
				experimentInfo = experimentInfo + "\nMinimum view count is set to " + minViewsCheck + ".";
			}

			experimentInfo = experimentInfo + "\n\nPlease check your connection and display settings before proceeding. Your device should not go to sleep during the experiment.";
			Database database = new Database();
			listOfVideoIDs = database.getListOfVideoIDs(databaseQuery);
			setContentView(R.layout.activity_youtube);
			TextView prepareText = (TextView) findViewById(R.id.prepareText);
			prepareText.setText(experimentInfo);
			webView = (WebView) this.findViewById(R.id.webView);
			WebSettings settings = webView.getSettings();
			settings.setJavaScriptEnabled(true);
			webView.setWebChromeClient(new MyChromeClient());
			webView.setWebViewClient(new MyWebviewClient());
			webView.loadUrl("file:///android_asset/ytplayer.html");
			long currentTime = System.currentTimeMillis();
			webAppInterface = new WebAppInterface(this, currentTime);
			webView.addJavascriptInterface(webAppInterface, "Android");
			webView.setWebContentsDebuggingEnabled(true);
			webAppInterface.setListener(new WebAppInterface.Listener() {
				@Override
				public void onLastVideoPlayed() {
					endExperiment();
				}
			});
		} else {
			Toast.makeText(this, "You must specify number of videos to start an experiment.", Toast.LENGTH_SHORT).show();
		}
	}

	public void load(View view)throws FileNotFoundException {

		WebView webView = (WebView) findViewById(R.id.webView);
		webView.setVisibility(View.VISIBLE);
		Button prepareButton = (Button) findViewById(R.id.button1);
		prepareButton.setVisibility(View.GONE);
		TextView prepareText = (TextView) findViewById(R.id.prepareText);
		prepareText.setVisibility(View.GONE);
		LinearLayout youqLayout = (LinearLayout) findViewById(R.id.youqLayout);
		youqLayout.setVisibility(View.GONE);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		webView.loadUrl("javascript:m_loadVideo('" + listOfVideoIDs + "')");
		webView.loadUrl("javascript:bufferPeriodically()");

	}

	public void endExperiment(){
		Integer numOfStallingE = webAppInterface.getnumOfStallingE();
		Float totalVideoDuration = webAppInterface.getTotalVideoDuration();
		Float experimentDuration = webAppInterface.getExperimentDuration();
		Integer numOfPlayedVideos = webAppInterface.getNumOfPlayedVideos();
		Intent intent = new Intent(this, EndActivity.class);
		intent.putExtra("numOfVid", numOfPlayedVideos);
		intent.putExtra("numOfStallingE", numOfStallingE);
		intent.putExtra("totalVideoDuration", totalVideoDuration);
		intent.putExtra("experimentDuration", experimentDuration);
		startActivity(intent);
		finish();
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {

		View v = getCurrentFocus();
		boolean ret = super.dispatchTouchEvent(event);

		if (v instanceof EditText) {
			View w = getCurrentFocus();
			int scrcoords[] = new int[2];
			w.getLocationOnScreen(scrcoords);
			float x = event.getRawX() + w.getLeft() - scrcoords[0];
			float y = event.getRawY() + w.getTop() - scrcoords[1];

			Log.d("Activity", "Touch event "+event.getRawX()+","+event.getRawY()+" "+x+","+y+" rect "+w.getLeft()+","+w.getTop()+","+w.getRight()+","+w.getBottom()+" coords "+scrcoords[0]+","+scrcoords[1]);
			if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom()) ) {

				InputMethodManager imm = (InputMethodManager)getSystemService(this.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
			}
		}
		return ret;
	}

	@Override
	public void onBackPressed(){
		if (webAppInterface != null){
			try {
				webAppInterface.finish();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		super.onBackPressed();
	}



	private class MyWebviewClient extends WebViewClient {

		@Override
		public void onReceivedError(WebView view, int errorCode,
									String description, String failingUrl) {
			// TODO Auto-generated method stub
			super.onReceivedError(view, errorCode, description, failingUrl);
			Log.d(TAG, "onReceivedError : description = " + description);

		}

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			// TODO Auto-generated method stub
			System.out.println("********************************************");
			Log.d(TAG, "shouldOverrideUrlLoading : url = " + url);
			return true;

		}

		@Override
		public void onLoadResource(WebView view, String url) {
			webAppInterface.logResourceURL(url);
			System.out.println("************************************ " + url + " ************************************");
		}
	}

	private class MyChromeClient extends WebChromeClient {
		@Override
		public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
			// TODO Auto-generated method stub
//          Log.d(TAG, "consoleMessage : " + consoleMessage.message());
			return super.onConsoleMessage(consoleMessage);
		}
	}
}
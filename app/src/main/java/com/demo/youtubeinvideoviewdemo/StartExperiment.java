package com.demo.youtubeinvideoviewdemo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

/**
 * Created by Illona on 22.11.2016..
 */

public class StartExperiment extends Activity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.start_experiment);
    }

    void startInBrowser (View view){

        startActivity(new Intent(this, YoutubeActivity.class));
    }
    void startInApp (View view){

        startActivity(new Intent(this, MainActivity.class));
    }
}

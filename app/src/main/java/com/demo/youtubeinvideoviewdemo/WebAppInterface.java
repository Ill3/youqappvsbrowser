package com.demo.youtubeinvideoviewdemo;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.telephony.CellInfo;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellInfoWcdma;
import android.telephony.TelephonyManager;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by irena01 on 7.11.2015..
 */
public class WebAppInterface {

    public interface Listener {
        void onLastVideoPlayed();
    }

    Boolean newVideo = true;

    Context mContext;
    FileOutputStream outputStreamE;
    FileOutputStream outputStreamB;
    FileOutputStream outputStreamU;

    Integer numOfPlayedVideos;
    Listener mListener;
    Integer numOfStallingE;
    Float totalVideoDuration;
    long firstEventTime;
    long lastEventTime;
    String currentVideo;
    Set playedVideos;
    WifiManager wifiManager;
    TelephonyManager telephonyManager;
    /** Instantiate the interface and set the context */
    WebAppInterface(Context c, long expStartTime) {
        mContext = c;
        currentVideo = new String();
        playedVideos = new HashSet<>();
        this.wifiManager = (WifiManager)mContext.getSystemService(Context.WIFI_SERVICE);
        this.telephonyManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        this.numOfPlayedVideos = 0;
        this.numOfStallingE = 0;
        this.totalVideoDuration = (float) 0;
        this.firstEventTime = 0;
        this.lastEventTime = 0;

        long filename = System.currentTimeMillis();
        String filenameE = "events" + expStartTime + ".csv";
        String filenameB = "buffer" + expStartTime + ".csv";
        String filenameU = "urls" + expStartTime + ".csv";
        mListener = null;
        try {
            outputStreamE = new FileOutputStream(new File(Environment.getExternalStorageDirectory().getAbsoluteFile() + "/tmp", filenameE));
            outputStreamB = new FileOutputStream(new File(Environment.getExternalStorageDirectory().getAbsoluteFile() + "/tmp", filenameB));
            outputStreamU = new FileOutputStream(new File(Environment.getExternalStorageDirectory().getAbsoluteFile() + "/tmp", filenameU));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        /*try {
            outputStreamE = new FileOutputStream(new File("/tmp/" + filenameE));
            outputStreamB = new FileOutputStream(new File("/tmp/" + filenameB));
            outputStreamU = new FileOutputStream(new File("/tmp/" + filenameU));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }*/
    }

    /** Show a toast from the web page */

    @JavascriptInterface
    public void handleEvent(String event) throws IOException {
        boolean lastVideo = false;
        int data = 0;
        int linkSpeed = wifiManager.getConnectionInfo().getRssi();
        Toast.makeText(mContext, event.split(",")[3], Toast.LENGTH_SHORT).show();
        try {
            final TelephonyManager tm = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
            for (final CellInfo info : tm.getAllCellInfo()) {
                if (info instanceof CellInfoGsm) {
                    data = ((CellInfoGsm) info).getCellSignalStrength().getDbm();
                    System.out.println("*********************** " + data);
                } else if (info instanceof CellInfoWcdma) {
                    data = ((CellInfoWcdma) info).getCellSignalStrength().getDbm();
                    System.out.println("*********************** " + data);
                } else if (info instanceof CellInfoLte) {
                    data = ((CellInfoLte) info).getCellSignalStrength().getDbm();
                    System.out.println("*********************** " + data);
                } else {
                    throw new Exception("Unknown type of cell signal!");
                }
            }
        } catch (Exception e) {

        }

        // Toast.makeText(mContext, numOfPlayedVideos.toString(), Toast.LENGTH_SHORT).show();
        if (firstEventTime == 0) firstEventTime= Long.valueOf(event.split(",")[1]);
        lastEventTime = Long.valueOf(event.split(",")[1]);
        if (event.split(",")[3].equals("Ended")){
            if(event.split(",")[4].equals("1"))
                lastVideo = true;
            event = event.substring(0,event.length()-2);
            numOfPlayedVideos++;
            totalVideoDuration += Float.valueOf(event.split(",")[2]);
        }
        if (event.split(",")[3].equals("Buffering") && !event.split(",")[2].equals("0.00")){
            numOfStallingE++;
        }
        currentVideo = event.split(",")[0];
        /*if (!playedVideos.contains(currentVideo)){
            Socket s = new Socket("192.168.1.101", 12345);
            playedVideos.add(currentVideo);
        }*/

        //if (event.split(",")[2].equals("0.00") && )
        try {
            outputStreamE.write((event + "," + linkSpeed + "," + data + "\n").getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (lastVideo){

            mListener.onLastVideoPlayed();
        }
    }



    @JavascriptInterface
    public void handleBuffer(String bufferState) {
        int data = 0;
        int linkSpeed = wifiManager.getConnectionInfo().getRssi();
        System.out.println("*******************************************");
        System.out.println(linkSpeed);
        try {
            final TelephonyManager tm = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
            for (final CellInfo info : tm.getAllCellInfo()) {
                if (info instanceof CellInfoGsm) {
                    data = ((CellInfoGsm) info).getCellSignalStrength().getDbm();
                    System.out.println("*********************** " + data);
                } else if (info instanceof CellInfoWcdma) {
                    data = ((CellInfoWcdma) info).getCellSignalStrength().getDbm();
                    System.out.println("*********************** " + data);
                } else if (info instanceof CellInfoLte) {
                    data = ((CellInfoLte) info).getCellSignalStrength().getDbm();
                    System.out.println("*********************** " + data);
                } else {
                    throw new Exception("Unknown type of cell signal!");
                }
            }
        } catch (Exception e) {

        }
        //Toast.makeText(mContext, bufferState, Toast.LENGTH_SHORT).show();
        try {
            outputStreamB.write((bufferState+","+linkSpeed+","+data+"\n").getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void logResourceURL(String url) {
        try {
            long currentTime = System.currentTimeMillis();
            outputStreamU.write(( currentTime + "|" + currentVideo + "|" + url + "\n").getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void finish() throws IOException {

        outputStreamE.close();
        outputStreamB.close();
        outputStreamU.close();
    }

    public Integer getnumOfStallingE() {
        return numOfStallingE;
    }

    public Float getTotalVideoDuration() {
        return totalVideoDuration;
    }

    public Float getExperimentDuration(){
        return (float)(lastEventTime-firstEventTime)/1000;
    }

    public Integer getNumOfPlayedVideos() {
        return numOfPlayedVideos;
    }

    public void setListener(Listener listener) {
        mListener = listener;
    }

}
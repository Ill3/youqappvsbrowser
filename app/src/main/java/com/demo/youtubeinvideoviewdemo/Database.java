package com.demo.youtubeinvideoviewdemo;


import android.os.AsyncTask;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;


/**
 * Created by pjuwy on 11/1/2015.
 */
public class Database{

    public Database() {

    }


    public String getListOfVideoIDs(JSONObject obj) throws IOException, URISyntaxException, JSONException {
        String listOfVideoIDs = "";
        String link = "http://161.53.19.92:80/query.php?";
        for(int i = 0; i<obj.names().length(); i++){
            if(i!=0)
                link = link + "&" + obj.names().getString(i) + "=" + obj.get(obj.names().getString(i));
            else
                link = link + obj.names().getString(i) + "=" + obj.get(obj.names().getString(i));
        }

        System.out.println(link);
        URI url = new URI(link);
        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet(url);
        //request.setURI(new URI(link));
        HttpResponse response = client.execute(request);
        BufferedReader in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

        StringBuffer sb = new StringBuffer("");
        String line="";

        while ((line = in.readLine()) != null) {
            sb.append(line);
            break;
        }
        in.close();
        String VideoIDs = sb.toString();
        int numOfCharacters = VideoIDs.length();
        int numOfReturnedIDs = numOfCharacters/11;
        String returnVideoIDs="";
        for(int i=0; i<numOfReturnedIDs; i++){
            if (i<numOfReturnedIDs-1) {
                returnVideoIDs =  returnVideoIDs + VideoIDs.substring(11*i,11*i+11);
                returnVideoIDs +=",";
            } else
                returnVideoIDs += VideoIDs.substring(11*i);
        }

        return returnVideoIDs;
    }
}